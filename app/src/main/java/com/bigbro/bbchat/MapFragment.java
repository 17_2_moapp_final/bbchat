package com.bigbro.bbchat;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    //GPS, Location 기능 on/off
    public boolean hasPermission = true;
    public boolean isGpsOn = true;
    public static int REQUEST_CODE_LOCATION = 1;
    //Google Map Variable
    private GoogleMap mMap;
    private MapView mapView = null;
    private Location lastKnownLocation = null;
    private LatLng defaultLatLng = new LatLng(35.890061, 128.611326);
    //CustomMarker Variable
    View marker_root_view;
    TextView tv_marker;
    ArrayList<Marker> array_friend_marker = new ArrayList<Marker>();
    Marker myMarker;
    //firebase database
    FirebaseDatabase database;
    String myuuid;
    String chatroomKey;
    ArrayList<String> memberList = new ArrayList<String>();
    ArrayList<Double> memberLati = new ArrayList<Double>();
    ArrayList<Double> memberLongi = new ArrayList<Double>();
    String templatitude;
    String templongitude;
    String temptime;
    String tempEmail;
    Context tempContext;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //firebase instance 얻기
        database = FirebaseDatabase.getInstance();
        tempContext = this.getContext();
        myuuid = ((ChatActivity) getActivity()).uid;
        chatroomKey = ((ChatActivity) getActivity()).getChatroomKey();
        //memberList 초기화
        init_memeberList();
        //CustomMarkerView를 설정합니다.
        setCustomMarkerView();

        if (!(isGpsOn = chkGpsService())) {
            Toast.makeText(getActivity(), "GPS 기능을 키고 다시 실행해주세요", Toast.LENGTH_LONG).show();
            Log.i("MapFragment", "GPS 기능을 켜주세요");
        }

        //FInd current location. 현재 위치를 찾자
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // 사용자 권한 요청
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
            Toast.makeText(getActivity(), "위치 권한을 허용하고 다시 실행해주세요", Toast.LENGTH_LONG).show();
            Log.i("MapFragment", "위치 권한을 허용해주세요");
            hasPermission = false;
        }

        if (!(hasPermission && isGpsOn)) {
            return;
        }

        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
            lastKnownLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListener);
            lastKnownLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if (lastKnownLocation == null) {
            lastKnownLocation = new Location("gps");
            //For Test - 현재위치를 경북대학교로
            lastKnownLocation.setLatitude(defaultLatLng.latitude);
            lastKnownLocation.setLongitude(defaultLatLng.longitude);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = (MapView) layout.findViewById(R.id.mapView);
        mapView.getMapAsync(this);

        return layout;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onLowMemory();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //액티비티가 처음 생성될 때 실행되는 함수
        if (mapView != null) {
            mapView.onCreate(savedInstanceState);
        }
    }

    //위치 정보와 관련된 일을 처리하는 리스너
    private LocationListener locationListener = new LocationListener() {

        //현재 위치가 변경될 경우
        @Override
        public void onLocationChanged(Location location) {
            try {
                LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                // Get the last location, and update UI.
                lastKnownLocation = location;
                //현재위치로 카메라 이동
                //moveCameraPositionToCurrentLocation();
                myMarker.remove();
                addMyMarker();

                DatabaseReference myRef = database.getReference("chatrooms")
                        .child(chatroomKey)
                        .child("members")
                        .child(myuuid)
                        .child("location");
                long now = System.currentTimeMillis();
                Date date = new Date(now);
                SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy.MM.dd.HH:mm");
                String formatDate = sdfNow.format(date);
                Map<String, Object> templocate = new Hashtable<>();
                templocate.put("latitude", Double.toString(lastKnownLocation.getLatitude()));
                templocate.put("longitude", Double.toString(lastKnownLocation.getLongitude()));
                templocate.put("time", formatDate);
                myRef.updateChildren(templocate);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //위치 권한이 없어서 내 현재 위치를 받아올 수 없는 경우.
        if (lastKnownLocation == null) {
            lastKnownLocation = new Location("gps");
            //For Test - 현재위치를 경북대학교로
            lastKnownLocation.setLatitude(defaultLatLng.latitude);
            lastKnownLocation.setLongitude(defaultLatLng.longitude);
        }
        //moveCameraPositionToCurrentLocation();

        DatabaseReference startrefer = database.getReference("chatrooms").child(chatroomKey).child("members");
        startrefer.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                memberLati.clear();
                memberLongi.clear();
                System.out.println(dataSnapshot.toString());
                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                    if (dataSnapshot2.getKey().equals(myuuid)) continue;
                    for (DataSnapshot dataSnapshot3 : dataSnapshot2.getChildren()) {
                        if (dataSnapshot3.getKey().equals("location")) {
                            for (DataSnapshot dataSnapshot4 : dataSnapshot3.getChildren()) {
                                if (dataSnapshot4.getKey().equals("latitude")) {
                                    if (Double.parseDouble(dataSnapshot4.getValue(String.class)) > 900)
                                        break;
                                    memberLati.add(Double.parseDouble(dataSnapshot4.getValue(String.class)));
                                } else if (dataSnapshot4.getKey().equals("longitude")) {
                                    if (Double.parseDouble(dataSnapshot4.getValue(String.class)) > 900)
                                        break;
                                    memberLongi.add(Double.parseDouble(dataSnapshot4.getValue(String.class)));
                                }

                            }
                            break;
                        }
                    }
                }

                System.out.println(memberList.size());
                double sumLati = lastKnownLocation.getLatitude();
                double sumLongi = lastKnownLocation.getLongitude();
                double distance = 0;

                for (int i = 0; i < memberLati.size(); i++) {
                    sumLati += memberLati.get(i);
                    sumLongi += memberLongi.get(i);
                }
                sumLati /= (1 + memberLati.size());
                sumLongi /= (1 + memberLati.size());


                for (int i = 0; i < memberLati.size(); i++) {
                    Location endLocation = new Location("gps");
                    endLocation.setLatitude(memberLati.get(i));
                    endLocation.setLongitude(memberLongi.get(i));
                    Location startLocation = new Location("gps");
                    startLocation.setLatitude(sumLati);
                    startLocation.setLongitude(sumLongi);
                    if (distance < startLocation.distanceTo(endLocation)) {
                        distance = startLocation.distanceTo(endLocation);
                    }
                }
                double baseDistance = 38.31482042;
                double baseZoom = 19;
                while (distance > baseDistance) {
                    baseDistance *= 2;
                    baseZoom--;
                }
                System.out.println(distance);
                if (distance == 0) baseZoom = 15.0;
                if (baseZoom < 19) baseZoom += 0.7;
                System.out.println("lati:" + sumLati + "\nlongi:" + sumLongi);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(sumLati, sumLongi), (float) (baseZoom)));
            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("addFriendMarker", "Failed to read value.", error.toException());
            }
        });

        //mMap.moveCamera(CameraUpdateFactory.zoomTo((float) 15.0));
        addMyMarker();

    }

    //GPS 설정 체크
    private boolean chkGpsService() {

        String gps = android.provider.Settings.Secure.getString(getActivity().getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        Log.d(gps, "GPS 설정 확인");
        if (!(gps.matches(".*gps.*") && gps.matches(".*network.*"))) {
            // GPS OFF 일때 Dialog 표시
            AlertDialog.Builder gsDialog = new AlertDialog.Builder(getActivity());
            gsDialog.setTitle("위치 서비스 설정");
            gsDialog.setMessage("무선 네트워크 사용, GPS 위성 사용을 모두 체크하셔야 정확한 위치 서비스가 가능합니다.\n위치 서비스 기능을 설정하시겠습니까?");
            gsDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // GPS설정 화면으로 이동
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    startActivity(intent);
                }
            })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    }).create().show();
            return false;

        } else {
            return true;
        }
    }

    //현재위치로 카메라 이동
    public void moveCameraPositionToCurrentLocation() {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude())));
    }

    //Custom Marker 생성
    private void setCustomMarkerView() {
        marker_root_view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_marker, null);
        tv_marker = (TextView) marker_root_view.findViewById(R.id.tv_marker);
    }

    private void addMyMarker() {
        LatLng position = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
        String email = ((ChatActivity) getActivity()).getCurrentUserEmail();
        tv_marker.setText(email.substring(0, email.indexOf("@")) + "\n" + email.substring(email.indexOf("@")));

        MarkerOptions markerOptions = new MarkerOptions();
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy.MM.dd.HH:mm");
        String formatDate = sdfNow.format(date);
        markerOptions.title(formatDate);
        markerOptions.position(position);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this.getActivity(), marker_root_view)));
        myMarker = mMap.addMarker(markerOptions);
    }

    private void addFriendMarker() {
        int i;
        for (i = 0; i < array_friend_marker.size(); i++) {
            array_friend_marker.get(i).remove();
        }
        array_friend_marker.clear();

        DatabaseReference myRef = database.getReference("chatrooms")
                .child(chatroomKey)
                .child("members");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                makeMemberMarker();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                makeMemberMarker();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                makeMemberMarker();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    // View를 Bitmap으로 변환
    private Bitmap createDrawableFromView(Context context, View view) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public void init_memeberList() {
        DatabaseReference myRef = database.getReference("chatrooms").child(chatroomKey).child("members");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue().toString();
                //Log.d("memberList changed", "Value is: " + value);

                memberList.clear();
                memberLati.clear();
                memberLongi.clear();
                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                    String value2 = dataSnapshot2.getValue().toString();
                    //Log.d("memberList changed", "Value is: " + value2);
                    if (dataSnapshot2.getKey().equals(myuuid)) continue;
                    memberList.add(dataSnapshot2.getKey());
                }

                addFriendMarker();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("memberList changed", "Failed to read value.", error.toException());
            }
        });
    }

    public void makeMemberMarker() {
        int i;
        for (i = 0; i < array_friend_marker.size(); i++) {
            array_friend_marker.get(i).remove();
        }
        array_friend_marker.clear();

        for (String tempMember : memberList) {

            DatabaseReference tempRefer = database.getReference("chatrooms")
                    .child(chatroomKey)
                    .child("members")
                    .child(tempMember);
            tempRefer.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                        if (dataSnapshot2.getKey().equals("email")) {
                            tempEmail = dataSnapshot2.getValue(String.class);
                        } else if (dataSnapshot2.getKey().equals("location")) {
                            for (DataSnapshot dataSnapshot3 : dataSnapshot2.getChildren()) {
                                if (dataSnapshot3.getKey().equals("latitude")) {
                                    templatitude = dataSnapshot3.getValue(String.class);
                                } else if (dataSnapshot3.getKey().equals("longitude")) {
                                    templongitude = dataSnapshot3.getValue(String.class);
                                } else if (dataSnapshot3.getKey().equals("time")) {
                                    temptime = dataSnapshot3.getValue(String.class);
                                }
                            }
                        }
                    }

                    if ((Math.abs(Double.parseDouble(templatitude)) > 90) || (Math.abs(Double.parseDouble(templongitude)) > 180))
                        return;

                    LatLng position = new LatLng(Double.parseDouble(templatitude), Double.parseDouble(templongitude));
                    String email = tempEmail;
                    tv_marker.setText(email.substring(0, email.indexOf("@")) + "\n" + email.substring(email.indexOf("@")));

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.title(temptime);
                    markerOptions.position(position);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(tempContext, marker_root_view)));
                    Marker tempMarker = mMap.addMarker(markerOptions);
                    //tempMarker.showInfoWindow();
                    array_friend_marker.add(tempMarker);
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("addFriendMarker", "Failed to read value.", error.toException());
                }
            });
        }
    }

}
