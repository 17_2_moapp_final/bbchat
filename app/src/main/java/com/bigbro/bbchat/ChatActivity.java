package com.bigbro.bbchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

public class ChatActivity extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    EditText etText;
    Button btnSend;
    String email;
    String uid;
    List<Chat> mChat;
    ArrayList<String> memberList; //친구 리스트
    FirebaseDatabase database;
    private boolean isScanning = false;
    //GoogleMap Fragment Variable
    BlankFragmentForGoogleMap frag_blank;
    MapFragment map_fragment = null;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    LinearLayout map_container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        isScanning = false;
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //DB open
        database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            email = user.getEmail();
            uid = user.getUid();
        }

        Intent in = getIntent();
        final String stChatId = in.getStringExtra("ChatroomUid");

        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference().child("chatrooms").child(stChatId).child("name");
        roomRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                actionBar.setTitle(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        initMember(stChatId);

        etText = (EditText) findViewById(R.id.etText);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String stText = etText.getText().toString();
                if (stText.equals("") || stText.isEmpty()) {
                    Toast.makeText(ChatActivity.this, "내용을 입력해 주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(ChatActivity.this, email + "," + stText, Toast.LENGTH_SHORT).show();


                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());


                    DatabaseReference myRef = database.getReference("chatrooms").child(stChatId).child("chat").child(formattedDate);

                    Hashtable<String, String> chat = new Hashtable<String, String>();
                    if (email == null) return;
                    chat.put("UID", uid);
                    chat.put("email", email);
                    chat.put("text", stText);
                    myRef.setValue(chat);
                    etText.setText("");
                }

            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mChat = new ArrayList<>();
        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(mChat, email, ChatActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        DatabaseReference myRef = database.getReference("chatrooms").child(stChatId).child("chat");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Chat chat = dataSnapshot.getValue(Chat.class);

                // [START_EXCLUDE]
                // Update RecyclerView

                mChat.add(chat);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.scrollToPosition(mChat.size() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Fragment create & set invisible
        frag_blank = new BlankFragmentForGoogleMap();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragGoogleMap, frag_blank);
        fragmentTransaction.commit();

        LinearLayout map_container = findViewById(R.id.fragGoogleMap);
        map_container.setVisibility(View.GONE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gps, menu);

        if (!isScanning) {
            menu.findItem(R.id.menu_gpsOn).setVisible(true);
            menu.findItem(R.id.menu_gpsOff).setVisible(false);

        } else {
            menu.findItem(R.id.menu_gpsOn).setVisible(false);
            menu.findItem(R.id.menu_gpsOff).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    //메뉴 버튼 선택 시
    public boolean onOptionsItemSelected(MenuItem item) {
        //homeButton [ <- ] 클릭 시
        if (item.getItemId() == android.R.id.home) {
            System.out.println("나 홈버튼 들어왔어!!");
            finish();
        }
        //gps_on/off_Button 클릭 시
        else {
            //처음 gps 버튼을 클릭할 시 gps map 생성.(권한 문제 설정을 위해 나눴음)
            if (map_fragment == null) {
                map_fragment = new MapFragment();
            }

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            map_container = findViewById(R.id.fragGoogleMap);
            if (map_container == null) System.out.println("map_container == null");

            switch (item.getItemId()) {
                case R.id.menu_gpsOn:
                    isScanning = true;
                    fragmentTransaction.replace(R.id.fragGoogleMap, map_fragment);
                    map_container.setVisibility(View.VISIBLE);
                    break;
                case R.id.menu_gpsOff:
                    isScanning = false;
                    map_container.setVisibility(View.GONE);
                    fragmentTransaction.replace(R.id.fragGoogleMap, frag_blank);
                    break;
            }
            fragmentTransaction.commit();
            invalidateOptionsMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    //chatting room의 사용자의 위치 정보를 가져오는 함수.
    public ArrayList<String> getMemberEmail() {
        return memberList;
    }
    public String getChatroomKey() {
        Intent in = getIntent();
        return in.getStringExtra("ChatroomUid");

    }

    public String getCurrentUserEmail() {
        return email;
    }

    public void initMember( String chatroomUid ) {
        memberList = new ArrayList<String>();

        DatabaseReference myRef = database.getReference("chatrooms").child(chatroomUid).child("members");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue().toString();
                Log.d(TAG, "Value is: " + value);

                memberList.clear();

                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                    String value2 = dataSnapshot2.getValue().toString();
                    Log.d(TAG, "Value is: " + value2);
                    if( dataSnapshot2.getKey() == uid ) continue;
                    memberList.add(dataSnapshot2.getKey());
                    System.out.println("member added : " + dataSnapshot2.getKey());
                    // [START_EXCLUDE]
                    // Update RecyclerView
                }

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }


}
