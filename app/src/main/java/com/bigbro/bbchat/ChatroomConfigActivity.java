package com.bigbro.bbchat;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

public class ChatroomConfigActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatroom_config);

        Intent in = getIntent();
        final String stChatId = in.getStringExtra("ChatroomUid");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("채팅방 설정");

        TextView tvChangeRoomName = (TextView) findViewById(R.id.tvChangeRoomName);
        tvChangeRoomName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ad = new AlertDialog.Builder(ChatroomConfigActivity.this);

                ad.setTitle("채팅방 이름 변경");       // 제목 설정

                // EditText 삽입하기
                final EditText et = new EditText(ChatroomConfigActivity.this);
                ad.setView(et);

                // 확인 버튼 설정
                ad.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (et.getText().toString().trim().length() == 0) {
                            Toast.makeText(ChatroomConfigActivity.this, "채팅방 이름 변경에 실패했습니다.", Toast.LENGTH_SHORT).show();
                        } else {
                            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("chatrooms").child(stChatId).child("name");
                            myRef.setValue(et.getText().toString());
                            Toast.makeText(ChatroomConfigActivity.this, "채팅방 이름을 변경했습니다.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();     //닫기
                    }
                });

                // 지우기 버튼 설정
                ad.setNeutralButton("지우기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                // 취소 버튼 설정
                ad.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();     //닫기
                    }
                });

                AlertDialog dialog = ad.create();
                dialog.show();
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        et.setText("");
                    }
                });
            }
        });

        TextView tvAddRoomMember = (TextView) findViewById(R.id.tvAddRoomMember);
        tvAddRoomMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ChatroomConfigActivity.this, AddMemberActivity.class);
                in.putExtra("ChatroomUid", stChatId);
                startActivity(in);
            }
        });

        TextView tvDeleteRoom = (TextView) findViewById(R.id.tvDeleteRoom);
        tvDeleteRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ad = new AlertDialog.Builder(ChatroomConfigActivity.this);

                ad.setTitle("채팅방 삭제");       // 제목 설정
                ad.setMessage("정말로 삭제하시겠습니까?");

                // 확인 버튼 설정
                ad.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("chatrooms").child(stChatId);
                        myRef.removeValue();
                        Toast.makeText(ChatroomConfigActivity.this, "채팅방을 삭제했습니다.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();     //닫기
                        finish();
                    }
                });

                ad.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                ad.show();
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
