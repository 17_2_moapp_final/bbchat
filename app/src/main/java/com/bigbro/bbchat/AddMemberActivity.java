package com.bigbro.bbchat;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

public class AddMemberActivity extends AppCompatActivity {

    FirebaseDatabase database;
    FirebaseUser user;
    DatabaseReference myRef;
    DatabaseReference memRef;

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    List<Friend> mFriend;
    FriendAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("채팅방 멤버 추가");

        Intent in = getIntent();
        final String roomID = in.getStringExtra("ChatroomUid");

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("chatrooms").child(roomID);

        user = FirebaseAuth.getInstance().getCurrentUser();
        memRef = myRef.child("members");

        mRecyclerView = (RecyclerView) findViewById(R.id.rvFriend_AddMember);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // 구분선 추가
        mRecyclerView.addItemDecoration(new DividerItemDecoration(AddMemberActivity.this, DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mFriend = new ArrayList<>();
        // specify an adapter (see also next example)
        mAdapter = new FriendAdapter(mFriend, this);
        mRecyclerView.setAdapter(mAdapter);

        final EditText etEmail = (EditText) findViewById(R.id.etEmail_AddMember);

        Button btnAddMember = (Button) findViewById(R.id.btnAddMember_AddMember);
        btnAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                if (email.trim().length() == 0) {
                    Toast.makeText(AddMemberActivity.this, "이메일 주소를 입력해주세요.", Toast.LENGTH_SHORT).show();

                    return;
                }

                DatabaseReference userRef = database.getReference("users");
                Query userQuery = userRef.orderByChild("email").equalTo(email);

                userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChildren()) {
                            for (DataSnapshot ss : dataSnapshot.getChildren()) {
                                Friend friend = new Friend();

                                String key = (String) ss.child("key").getValue();
                                String email = (String) ss.child("email").getValue();
                                String photo = (String) ss.child("photo").getValue();

                                friend.setKey(key);
                                friend.setEmail(email);
                                friend.setPhoto(photo);

                                mFriend.add(friend);
                            }

                            mAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(AddMemberActivity.this, "존재하지 않는 사용자입니다.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                etEmail.setText("");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_makeroom, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_confirm:
                for (int i = 0; i < mFriend.size(); i++) {
                    Query memQuery = memRef.orderByChild("key").equalTo(mFriend.get(i).getKey());
                    final int index = i;

                    memQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() == 0) {
                                Hashtable<String, String> memInfo = new Hashtable<String, String>();

                                memInfo.put("email", mFriend.get(index).getEmail());
                                memInfo.put("key", mFriend.get(index).getKey());
                                memRef.child(mFriend.get(index).getKey()).setValue(memInfo);
                                //location 추가
                                long now = System.currentTimeMillis();
                                // 현재시간을 date 변수에 저장한다.
                                Date date = new Date(now);
                                SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy.MM.dd.HH:mm");
                                String formatDate = sdfNow.format(date);
                                Hashtable<String, String> locationData = new Hashtable<String, String>();
                                locationData.put("latitude", "999");
                                locationData.put("longitude", "999");
                                locationData.put("time", formatDate);
                                memRef.child(mFriend.get(index).getKey()).child("location").setValue(locationData);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
