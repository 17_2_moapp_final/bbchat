package com.bigbro.bbchat;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ChatroomAdapter extends RecyclerView.Adapter<ChatroomAdapter.ViewHolder> {
    List<Chatroom> mChatroom;
    Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvName;
        public ImageView ivChatroom;
        public RelativeLayout ctChatroom;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivChatroom = (ImageView) itemView.findViewById(R.id.ivChatroom);
            ctChatroom = (RelativeLayout) itemView.findViewById(R.id.ctChatroom);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatroomAdapter(List<Chatroom> mChatroom, Context context) {
        this.mChatroom = mChatroom;
        this.context = context;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public ChatroomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_chatroom, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ChatroomAdapter.ViewHolder vh = new ChatroomAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ChatroomAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvName.setText(mChatroom.get(position).getName());

        String stPhoto = mChatroom.get(position).getPhoto();
        if (TextUtils.isEmpty(stPhoto)) {

            Picasso.with(context)
                    .load(R.mipmap.ic_nophoto)
                    .fit()
                    .centerInside()
                    .into(holder.ivChatroom);

        } else {
            Picasso.with(context)
                    .load(stPhoto)
                    .fit()
                    .centerInside()
                    .into(holder.ivChatroom);
        }

        holder.ctChatroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stChatroomId = mChatroom.get(position).getKey();
                Intent in = new Intent(context, ChatActivity.class);
                in.putExtra("ChatroomUid", stChatroomId);
                context.startActivity(in);
            }
        });

        holder.ctChatroom.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String stChatroomId = mChatroom.get(position).getKey();
                Intent in = new Intent(context, ChatroomConfigActivity.class);
                in.putExtra("ChatroomUid", stChatroomId);
                context.startActivity(in);

                return true;
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mChatroom.size();
    }
}
