package com.bigbro.bbchat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/*
최초 실행 시 또는 채팅방 실행 후
지도 기능을 사용하지 않을 경우에
지도 권한을 요청하지 않고,
구글맵 기능을 사용하지 않기 위해
생성한 레이아웃
 */

public class BlankFragmentForGoogleMap extends Fragment {


    public BlankFragmentForGoogleMap() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank_for_google_map, container, false);
    }

}
