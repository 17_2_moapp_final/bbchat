package com.bigbro.bbchat;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatroomsFragment extends Fragment {

    String TAG = getClass().getSimpleName();

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;

    List<Chatroom> mChatroom;

    FirebaseDatabase database;
    ChatroomAdapter mAdapter;
    String email;
    String uid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            email = user.getEmail();
            uid = user.getUid();

        }
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chatrooms, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.rvChatroom);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // 구분선 추가
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mChatroom = new ArrayList<>();
        // specify an adapter (see also next example)
        mAdapter = new ChatroomAdapter(mChatroom, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLongClickable(true);

        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("chatrooms");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue().toString();
                Log.d(TAG, "Value is: " + value);

                mChatroom.clear();
                //each chatroom
                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                    //find member
                    for ( DataSnapshot dataSnapshot3 : dataSnapshot2.getChildren() ) {
                        if( dataSnapshot3.getKey().equals("members") ) {
                            //each member
                            for( DataSnapshot dataSnapshot4 : dataSnapshot3.getChildren() ) {
                                if ( dataSnapshot4.getKey().equals(uid) ) {
                                    String value2 = dataSnapshot2.getValue().toString();
                                    Log.d(TAG, "Value is: " + value2);
                                    Chatroom chatroom = dataSnapshot2.getValue(Chatroom.class);

                                    // [START_EXCLUDE]
                                    // Update RecyclerView

                                    mChatroom.add(chatroom);
                                }
                            }
                        }
                    }
                }

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        FloatingActionButton btnAddRoom = (FloatingActionButton) v.findViewById(R.id.addRoomBtn);
        btnAddRoom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent in = new Intent(getContext(), MakeRoomActivity.class);
                startActivity(in);
            }
        });

        return v;
    }
}
