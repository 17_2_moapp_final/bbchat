package com.bigbro.bbchat;

public class Chatroom {

    public String name;

    public String photo;

    public String key;

    public Chatroom() {
        // Default constructor required for calls to DataSnapshot.getValue(Comment.class)
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
